E-Manage System

Main purpose: Manage your business

Roles:

1. Agent -> He can access his sales and add sales, he can check his clients, edit them and add new one. And he can see his performance report. He can see his profile.
2. Manager -> He can access the total sales made in a big table. He can edit the sales. He can check his agents and check their performance in graphs and more details for his products.
           -> He can also access all the products. Add new ones, search them, and check the sales for each specific product.
           -> He can pay the agents, check all the payment given in a pie graph
3. Admin -> Admin has editing role. He doesn't access the sales and products because manager takes care of that. 
         -> Admin can add users, and create their profile as an agent or manager. He can also edit the current accounts existing.


Website is located at: 
stud-proj.epoka.edu.al/~istergu17/project
and 
stud-proj.epoka.edu.al/~oballa16/project


Access:
1. Agent:
Username: orges
Password: q

2. Manager
Username: igli
Password: q
3. Admin
Username: admin
Password: q