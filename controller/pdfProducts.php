<?php
include "../controller/db.php";
include "../model/fpdf.php";
require "../model/pdf.php";
$pdf = new pdf();
$pdf->AddPage('L');
$pdf->SetFont('Arial', 'B', 10);
$currentDate = date("j/n/Y");


$pdf->SetFont('Arial', 'B', 14);

//Cell(width , height , text , border , end line , [align] )

$pdf->Cell(130, 5, 'E-Manage', 0, 0);
$pdf->Cell(59, 5, 'Report', 0, 1);//end of line

//set font to arial, regular, 12pt
$pdf->SetFont('Arial', '', 12);

$pdf->Cell(130, 5, 'Rr. Komuna Parisit', 0, 0);
$pdf->Cell(59, 5, '', 0, 1);//end of line

$pdf->Cell(130, 5, 'Tirane, Albania, 1000', 0, 0);
$pdf->Cell(25, 5, 'Date', 0, 0);
$pdf->Cell(34, 5, $currentDate, 0, 1);//end of line

$pdf->Cell(130, 5, 'Phone +12345678', 0, 0);
$pdf->Cell(25, 5, 'Report #', 0, 0);
$pdf->Cell(34, 5, 'My Products', 0, 1);//end of line

$pdf->Cell(130, 5, 'Fax [+12345678]', 0, 0);
$pdf->Cell(25, 5, 'Customer ID', 0, 0);
$pdf->Cell(34, 5, '[1234567]', 0, 1);//end of line

$pdf->Ln();
$pdf->Ln();
$pdf->Ln();

$header = array('ID', 'Code', 'Brand', 'Details', 'Unit', 'Bought Price', 'Sold Price', 'Stock', 'Min. Quantity');
$sql = "SELECT * from produkte";
$res = $conn->query($sql);
$i = 10;
$j = 26;
$pdf->Cell(10, 10, $header[0], 1, 0, 'C');
for ($x = 1; $x < 9; $x++) {
        $pdf->Cell($j, $i, $header[$x], 1, 0, 'C');
}
$pdf->Ln();

while ($row = mysqli_fetch_array($res)) {
    $id = $row['id'];
    $kodi = $row['Kodi'];
    $brandi = $row['Brandi'];
    $details = $row['Pershkrimi'];
    $unit = $row['Njesia'];
    $Cbl = $row['Cbl'];
    $Csh = $row['Csh'];
    $sasigj = $row['Sasigj'];
    $moq = $row['Moq'];

    $pdf->Cell(10, 10, $id, 'LRB');
    $pdf->Cell($j, $i, $kodi, 'LRB');
    $pdf->Cell($j, $i, $brandi, 'LRB');
    $pdf->Cell($j, $i, $details, 'LRB');
    $pdf->Cell($j, $i, $unit, 'LRB');
    $pdf->Cell($j, $i, $Cbl, 'LRB');
    $pdf->Cell($j, $i, $Csh, 'LRB');
    $pdf->Cell($j, $i, $sasigj, 'LRB');
    $pdf->Cell($j, $i, $moq, 'LRB');
    $pdf->Ln();
}
$pdf->Output();
header("Content-type: application/pdf");
header("Content-Length: 500");
header("Content-Disposition: inline; filename=ProductsReport.pdf");