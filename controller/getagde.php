<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
include "db.php";
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
$result = $conn->query("SELECT * FROM Agjent");
$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    if($rs["ctgbon"] == 0) {
        $rs["ctgbon"] = 'Pa përqindje';
    }
    else{
        $rs["ctgbon"] = 'Me përqindje';
    }
    $outp .= '{"Emri":"'  . $rs["emri"] . '","Mbiemri":"'  . $rs["mbiemri"] . '","Bonusi":"'  . $rs["bonusi"] . '","Lloji i Pageses":"' . $rs["ctgbon"] . '","Nr. Celulari":"'  . $rs["nrtel"] . '","Iban":"'  . $rs["iban"] . '","ID":"'  . $rs["id2"] .'"}';
}
$outp .= "]";
$outf = "[" .$outp;
echo($outf);
$conn->close();
?>