<?php
include "../controller/db.php";
include "../model/fpdf.php";
require "../model/pdf.php";
$pdf = new pdf();
$pdf->AddPage('L');
$pdf->SetFont('Arial','B',10);
$currentDate = date("j/n/Y");


$pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )

$pdf->Cell(130 ,5,'E-Manage',0,0);
$pdf->Cell(59 ,5,'Report',0,1);//end of line

//set font to arial, regular, 12pt
$pdf->SetFont('Arial','',12);

$pdf->Cell(130 ,5,'Rr. Komuna Parisit',0,0);
$pdf->Cell(59 ,5,'',0,1);//end of line

$pdf->Cell(130 ,5,'Tirane, Albania, 1000',0,0);
$pdf->Cell(25 ,5,'Date',0,0);
$pdf->Cell(34 ,5,$currentDate,0,1);//end of line

$pdf->Cell(130 ,5,'Phone +12345678',0,0);
$pdf->Cell(25 ,5,'Report #',0,0);
$pdf->Cell(34 ,5,'Total Sales',0,1);//end of line

$pdf->Cell(130 ,5,'Fax [+12345678]',0,0);
$pdf->Cell(25 ,5,'Customer ID',0,0);
$pdf->Cell(34 ,5,'[1234567]',0,1);//end of line

$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$header = array('id3', 'Date of Sale', 'Company', 'NIPT','Code','Brand','Stock','Quantity sold','Sold Price','Agent Name','Surname');
$sql = "SELECT shitje.id3,produkte.Sasigj,shitje.datash,shitje.sasi,shitje.cmimi,shitje.vlere,agjent.emri,agjent.mbiemri,produkte.Kodi,produkte.Brandi,klient.Kompania,klient.Nipt FROM `shitje` INNER JOIN agjent ON shitje.agjenti=agjent.id2 INNER JOIN klient ON shitje.klienti=klient.id1 INNER JOIN produkte ON shitje.produkti=produkte.id";
$res = $conn->query($sql);
$i=10;
$j=25;
for($x=0;$x<11;$x++){
    $pdf->Cell($j,$i,$header[$x],1,0,'C');
}
$pdf->Ln();
while($row = mysqli_fetch_array($res)){
    $id=$row['id3'];
    $datash = $row['datash'];
    $kompania = $row['Kompania'];
    $nipt = $row['Nipt'];
    $kodi = $row['Kodi'];
    $brand = $row['Brandi'];
    $sasigj = $row['Sasigj'];
    $sasi = $row['sasi'];
    $cmimi = $row['cmimi'];
    $vlere = $row['vlere'];
    $emri = $row['emri'];
    $mbiemri = $row['mbiemri'];

    $pdf->Cell($j,$i,$id,'LRB');
    $pdf->Cell($j,$i,$datash,'LRB');
    $pdf->Cell($j,$i,$kompania,'LRB');
    $pdf->Cell($j,$i,$kodi,'LRB');
    $pdf->Cell($j,$i,$brand,'LRB');
    $pdf->Cell($j,$i,$sasigj,'LRB');
    $pdf->Cell($j,$i,$sasi,'LRB');
    $pdf->Cell($j,$i,$cmimi,'LRB');
    $pdf->Cell($j,$i,$vlere,'LRB');
    $pdf->Cell($j,$i,$emri,'LRB');
    $pdf->Cell($j,$i,$mbiemri,'LRB');
    $pdf->Ln();
}
$pdf->Output();
header("Content-type: application/pdf");
header("Content-Length: 500");
header("Content-Disposition: inline; filename=hello.pdf");