<?php
session_start();
include "../controller/db.php";

$sql="SELECT SUM(vlera) as total ,agjent.emri FROM `bonus`,agjent where agjenti = agjent.id2 GROUP BY agjenti";
$result = $conn->query($sql);
$dataPoints = array();
while ($row = mysqli_fetch_assoc($result)) {
    $arr = array("y" => $row['total'],"label"=>$row['emri']);
    array_push($dataPoints,$arr);
}
echo json_encode($dataPoints,JSON_NUMERIC_CHECK);
?>