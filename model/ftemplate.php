<?php
echo '<footer>
<div class="footer w3ls light">
    <div class="container">
        <div class="footer-main">
            <div class="footer-top">
            <div class="flexing" style="margin-left: 250px; margin-bottom: 0px">
                <div class="col-md-4 ftr-grid" style="width: 33%">
                    <h3>E-Manage</h3>
                    <p>Manage your business remote. With our system you can manage total sales, manage your products, agents and check agent performances by doing everything online and remote. Brought to you by Igli and Orges.</p>
                </div>
                <div class="col-md-4 ftr-grid" style="width: 33%;">
                    <h3>Contact</h3>
                    <div class="ftr-address">
                        <div class="local">
                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                        </div>
                            <p>Email @ oballa16@epoka.edu.al</p>
                            <p>Email @ istergu17@epoka.edu.al</p>
                        
                        <div class="clearfix"> </div>
                    </div>
                    <div class="ftr-address">
                        <div class="local">
                            <span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
                        </div>
                       
                            <p>+355 123456789</p>
                 
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-4 ftr-grid" style="width: 33%;">
                    <h3>Stay Tuned</h3>
                    <p>More coming up soon....</p>
                    <p>All rights reserved</p>
                </div>
                <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
</div></footer>';