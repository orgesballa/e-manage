var butclconc = [0,0,0,0,0,0,0,0,0];
var saved = 0;
var valu;
function selSort(n,type){
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("table1");
    switching = true;
    dir = "asc";
    var coln = n;
    if(type == 0){
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                if (dir == "asc") {
                    if (parseInt(x.innerHTML) > (y.innerHTML)) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (parseInt(x.innerHTML) < parseInt(y.innerHTML)) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }}
    else{
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length-1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }
    if(butclconc[parseInt(coln)] == 0){
        butclconc[0] = 0;
        var th = document.getElementsByTagName("th")[0].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[1] = 0;
        var th = document.getElementsByTagName("th")[1].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[2] = 0;
        var th = document.getElementsByTagName("th")[2].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[3] = 0;
        var th = document.getElementsByTagName("th")[3].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[4] = 0;
        var th = document.getElementsByTagName("th")[4].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[5] = 0;
        var th = document.getElementsByTagName("th")[5].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[6] = 0;
        var th = document.getElementsByTagName("th")[6].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[7] = 0;
        var th = document.getElementsByTagName("th")[7].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[parseInt(coln)] = 1;
        console.log("des");
        var th = document.getElementsByTagName("th")[coln].lastChild;
        if(coln == 0 || coln == 5){
            th.attributes[1].value = "../resources/des2.png";
        }
        else{
            th.attributes[1].value = "../resources/asc2.png";
        }

    }
    else {
        butclconc[0] = 0;
        butclconc[1] = 0;
        butclconc[2] = 0;
        butclconc[3] = 0;
        butclconc[4] = 0;
        butclconc[5] = 0;
        butclconc[6] = 0;
        butclconc[7] = 0;
        console.log("asc");
        var th = document.getElementsByTagName("th")[coln].lastChild;
        if(coln == 0 || coln == 5){
            th.attributes[1].value = "../resources/asc2.png";
        }
        else{
            th.attributes[1].value = "../resources/des2.png";
        }

    }

}
function save(rw1){
    var i = rw1.parentNode;
    var colm = rw1.parentNode.cellIndex;
    var y = document.getElementById("txt20").value;
    if(colm == 1 ){
        if( validate(y) == 4){
            i.innerHTML = "";
            i.innerText = y;
            saved = 1;
            i.setAttribute("onclick","changecell(this)");
            var id = [];
            id[0] = i.parentNode.childNodes[1].innerText;
            id[1] = i.parentNode.childNodes[2].innerText;
            id[2] = i.parentNode.childNodes[3].innerText;
            id[3] = i.parentNode.childNodes[4].innerText;
            id[4] = i.parentNode.childNodes[5].innerText;
            id[5] = i.parentNode.childNodes[6].innerText;
            id[6] = i.parentNode.childNodes[7].innerText;
            id[7] = i.parentNode.childNodes[8].innerText;
            var xhtml = new XMLHttpRequest();
            xhtml.onreadystatechange = function () {
                if(this.readyState === 4 && this.status === 200) {
                    console.log(this.response);
                }
            };
            xhtml.open('POST','../controller/Update/updAgj.php', true);
            var xtl='d0='+id[0]+'&d1='+id[1]+'&d2='+id[2]+'&d3='+id[3]+'&d4='+id[4]+'&d5='+id[5]+'&d6='+id[6]+'&d7='+id[7];
            xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhtml.send(xtl);
        }
        else{
            alert("Must be alphabetic");
        }
    }
    else if(colm==2){
        if( validate(y) == 4){
            i.innerHTML = "";
            i.innerText = y;
            saved = 1;
            i.setAttribute("onclick","changecell(this)");
            var id = [];
            id[0] = i.parentNode.childNodes[1].innerText;
            id[1] = i.parentNode.childNodes[2].innerText;
            id[2] = i.parentNode.childNodes[3].innerText;
            id[3] = i.parentNode.childNodes[4].innerText;
            id[4] = i.parentNode.childNodes[5].innerText;
            id[5] = i.parentNode.childNodes[6].innerText;
            id[6] = i.parentNode.childNodes[7].innerText;
            id[7] = i.parentNode.childNodes[8].innerText;
            var xhtml = new XMLHttpRequest();
            xhtml.onreadystatechange = function () {
                if(this.readyState === 4 && this.status === 200) {
                    console.log(this.response);
                }
            };
            xhtml.open('POST','../controller/Update/updAgj.php', true);
            var xtl='d0='+id[0]+'&d1='+id[1]+'&d2='+id[2]+'&d3='+id[3]+'&d4='+id[4]+'&d5='+id[5]+'&d6='+id[6]+'&d7='+id[7];
            xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhtml.send(xtl);
        }
        else{
            alert("Must be alphanumeric");
        }
    }
    else if(colm==3){
        if( validate(y) == 6){
            i.innerHTML = "";
            i.innerText = y;
            saved = 1;
            i.setAttribute("onclick","changecell(this)");
            var id = [];
            id[0] = i.parentNode.childNodes[1].innerText;
            id[1] = i.parentNode.childNodes[2].innerText;
            id[2] = i.parentNode.childNodes[3].innerText;
            id[3] = i.parentNode.childNodes[4].innerText;
            id[4] = i.parentNode.childNodes[5].innerText;
            id[5] = i.parentNode.childNodes[6].innerText;
            id[6] = i.parentNode.childNodes[7].innerText;
            id[7] = i.parentNode.childNodes[8].innerText;
            var xhtml = new XMLHttpRequest();
            xhtml.onreadystatechange = function () {
                if(this.readyState === 4 && this.status === 200) {
                    console.log(this.response);
                }
            };
            xhtml.open('POST','../controller/Update/updAgj.php', true);
            var xtl='d0='+id[0]+'&d1='+id[1]+'&d2='+id[2]+'&d3='+id[3]+'&d4='+id[4]+'&d5='+id[5]+'&d6='+id[6]+'&d7='+id[7];
            xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhtml.send(xtl);
        }
        else{
            alert("Date not valid");
        }
    }
    else if(colm==5){
        if( validate(y) == 1){
            i.innerHTML = "";
            i.innerText = y;
            saved = 1;
            i.setAttribute("onclick","changecell(this)");
            var id = [];
            id[0] = i.parentNode.childNodes[1].innerText;
            id[1] = i.parentNode.childNodes[2].innerText;
            id[2] = i.parentNode.childNodes[3].innerText;
            id[3] = i.parentNode.childNodes[4].innerText;
            id[4] = i.parentNode.childNodes[5].innerText;
            id[5] = i.parentNode.childNodes[6].innerText;
            id[6] = i.parentNode.childNodes[7].innerText;
            id[7] = i.parentNode.childNodes[8].innerText;
            var xhtml = new XMLHttpRequest();
            xhtml.onreadystatechange = function () {
                if(this.readyState === 4 && this.status === 200) {
                    console.log(this.response);
                }
            };
            xhtml.open('POST','../controller/Update/updAgj.php', true);
            var xtl='d0='+id[0]+'&d1='+id[1]+'&d2='+id[2]+'&d3='+id[3]+'&d4='+id[4]+'&d5='+id[5]+'&d6='+id[6]+'&d7='+id[7];
            xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhtml.send(xtl);
        }
        else{
            alert("Must be numbersw");
        }
    }
    else if(colm==6){
        if( validate(y) == 5){
            i.innerHTML = "";
            i.innerText = y;
            saved = 1;
            i.setAttribute("onclick","changecell(this)");
            var id = [];
            id[0] = i.parentNode.childNodes[1].innerText;
            id[1] = i.parentNode.childNodes[2].innerText;
            id[2] = i.parentNode.childNodes[3].innerText;
            id[3] = i.parentNode.childNodes[4].innerText;
            id[4] = i.parentNode.childNodes[5].innerText;
            id[5] = i.parentNode.childNodes[6].innerText;
            id[6] = i.parentNode.childNodes[7].innerText;
            id[7] = i.parentNode.childNodes[8].innerText;
            var xhtml = new XMLHttpRequest();
            xhtml.onreadystatechange = function () {
                if(this.readyState === 4 && this.status === 200) {
                    console.log(this.response);
                }
            };
            xhtml.open('POST','../controller/Update/updAgj.php', true);
            var xtl='d0='+id[0]+'&d1='+id[1]+'&d2='+id[2]+'&d3='+id[3]+'&d4='+id[4]+'&d5='+id[5]+'&d6='+id[6]+'&d7='+id[7];
            xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhtml.send(xtl);
        }
        else{
            alert("Must be IBAN format");
        }
    }
}
function cancel(rw2){
    var i = rw2.parentNode;
    i.innerHTML = "";
    i.innerText = valu;
    i.setAttribute("onclick","changecell(this)");
    saved=1;
    location.reload();
}
function changecell(rw){
    if(saved != 1){
        var i = rw;
        valu = i.innerText;
        i.removeAttribute("onclick");
        i.innerHTML = "<input id='txt20' type='text'>" + "</br>" + "<a onclick='save(this)' href=\"#\" style='color: darkcyan'>Save</a>    "  + "<a style='color: darkcyan' onclick='cancel(this)' href=\"#\">Cancel</a>";
    }
    saved = 0;
}
function validate(inppp){
    console.log(inppp);
    var letters = /^[A-Za-z\s\.]+$/;
    var alfnum = /^[A-Za-z0-9\s\.]+$/;
    var date = /^\d{1,5}[/-][0-1][0-9][/-][0-3][0-9]$/;
    var iban = /^AL(0){5}\d{5}$/;
    var inp = inppp;
    if(!isNaN(inp)){
        return 1; //"Is number"
    } else if(inp.length==0){
        return 2 ; //Is empty
    } else if(inp.match(letters)){
        return 4 ; //Is alphabetic
    }
    else if(inp.match(iban)){
        return 5 ; //Is iban
    }
    else if(inp.match(date)){
        return 6 ; //Is date
    }
    else{return 7 ;} //dont know
}
function Search(){
    var e = document.getElementById("type");
    var type = e.options[e.selectedIndex].value;
    var val = document.getElementById("txt11").value;
    var xhtml = new XMLHttpRequest();
    xhtml.onreadystatechange = function () {
        if(this.readyState === 4 && this.status === 200) {
            var resp = this.response;
            var x = document.getElementById("table1").rows.length;
            for(var z = 1;z<x;z++){
                document.getElementById("table1").deleteRow(1);
            }
            document.getElementById("table1").innerHTML+=resp;
            document.getElementById('search').style.display='none';
        }
    };
    xhtml.open('POST','../controller/Search/agjentsearch.php', true);
    var xtl='d1='+type+'&d2='+val;
    xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhtml.send(xtl);
}
function detajeRW(rw3){
    var id = rw3.parentNode.parentNode.childNodes[1].innerText;
    console.log(id);
    document.cookie = 'pid='+id;
    location.href='../view/detagj.php';
}