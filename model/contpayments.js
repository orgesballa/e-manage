function pay() {
    var id = document.getElementById("AgID").value;
    var value = document.getElementById("AgPay").value;

    var xhtml = new XMLHttpRequest();
    xhtml.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var response = xhtml.responseText;
            console.log(response);
            if (response == "OK") {
                document.getElementById("pay").style.display = 'none';
                location.reload();
            }else{
                document.getElementById("error").innerText = "Invalid ID";
            }
        }
    };
    xhtml.open('POST', '../controller/Insert/insSalary.php', true);
    var xtl = 'd1=' + id + '&d2=' + value;
    xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhtml.send(xtl);
}
var butclconc = [0,0,0,0,0,0];
function selSort(n,type){
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("table1");
    switching = true;
    dir = "asc";
    var coln = n;
    if(type == 0){
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                if (dir == "asc") {
                    if (parseInt(x.innerHTML) > (y.innerHTML)) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (parseInt(x.innerHTML) < parseInt(y.innerHTML)) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }}
    else{
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length-1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }
    if(butclconc[parseInt(coln)] == 0){
        butclconc[0] = 0;
        var th = document.getElementsByTagName("th")[0].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[1] = 0;
        var th = document.getElementsByTagName("th")[1].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[2] = 0;
        var th = document.getElementsByTagName("th")[2].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[3] = 0;
        butclconc[parseInt(coln)] = 1;
        console.log("des");
        var th = document.getElementsByTagName("th")[coln].lastChild;
        if(coln == 0 || coln == 5){
            th.attributes[1].value = "../resources/des2.png";
        }
        else{
            th.attributes[1].value = "../resources/asc2.png";
        }

    }
    else {
        butclconc[0] = 0;
        butclconc[1] = 0;
        butclconc[2] = 0;
        butclconc[3] = 0;
        butclconc[4] = 0;
        console.log("asc");
        var th = document.getElementsByTagName("th")[coln].lastChild;
        if(coln == 0 || coln == 5){
            th.attributes[1].value = "../resources/asc2.png";
        }
        else{
            th.attributes[1].value = "../resources/des2.png";
        }

    }

}