var butclconc = [0,0,0,0];

/*Delete User  Done */
function deleteRW(rw) {
    var i = rw.parentNode.parentNode.childNodes[1].innerHTML;
    var j = rw.parentNode.parentNode.rowIndex;
    document.getElementById("table1").deleteRow(parseInt(j));
    var xhtml = new XMLHttpRequest();
    xhtml.onreadystatechange = function () {
        if(this.readyState === 4 && this.status === 200) {
            console.log(this.response);
        }
    };
    xhtml.open('POST','../controller/Delete/delUser.php', true);
    var xtl='id='+i;
    xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhtml.send(xtl);
}
function selSort(n,type){
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("table1");
    switching = true;
    dir = "asc";
    var coln = n;
    if(type == 0){
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                if (dir == "asc") {
                    if (parseInt(x.innerHTML) > (y.innerHTML)) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (parseInt(x.innerHTML) < parseInt(y.innerHTML)) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }}
    else{
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length-1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount ++;
            } else {
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }
    if(butclconc[parseInt(coln)] == 0){
        butclconc[0] = 0;
        var th = document.getElementsByTagName("th")[0].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[1] = 0;
        var th = document.getElementsByTagName("th")[1].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[2] = 0;
        var th = document.getElementsByTagName("th")[2].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[3] = 0;
        butclconc[parseInt(coln)] = 1;
        console.log("des");
        var th = document.getElementsByTagName("th")[coln].lastChild;
        if(coln == 0 || coln == 5){
            th.attributes[1].value = "../resources/des2.png";
        }
        else{
            th.attributes[1].value = "../resources/asc2.png";
        }

    }
    else {
        butclconc[0] = 0;
        butclconc[1] = 0;
        butclconc[2] = 0;
        butclconc[3] = 0;
        console.log("asc");
        var th = document.getElementsByTagName("th")[coln].lastChild;
        if(coln == 0 || coln == 5){
            th.attributes[1].value = "../resources/asc2.png";
        }
        else{
            th.attributes[1].value = "../resources/des2.png";
        }

    }

}

function save(rw1){
    var i = rw1.parentNode;
    var colm = rw1.parentNode.cellIndex;
    var y = document.getElementById("txt8").value;
    if(colm == 1 ){
        if( validate(y) == 3){
            i.innerHTML = "";
            i.innerText = y;
            saved = 1;
            i.setAttribute("onclick","changecell(this)");
            var id = [];
            id[0] = i.parentNode.childNodes[1].innerText;
            id[1] = i.parentNode.childNodes[2].innerText;
            id[2] = i.parentNode.childNodes[3].innerText;
            id[3] = i.parentNode.childNodes[4].innerText;
            var xhtml = new XMLHttpRequest();
            xhtml.onreadystatechange = function () {
                if(this.readyState === 4 && this.status === 200) {
                    console.log(this.response);
                }
            };
            xhtml.open('POST','../controller/Update/updUser.php', true);
            var xtl='d0='+id[0]+'&d1='+id[1]+'&d2='+id[2]+'&d3='+id[3];
            xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhtml.send(xtl);
        }
        else{
            alert("Must be alphanumeric");
        }
    }
    else if(colm==2){
        if( validate(y) == 3){
            i.innerHTML = "";
            i.innerText = y;
            saved = 1;
            i.setAttribute("onclick","changecell(this)");
            var id = [];
            id[0] = i.parentNode.childNodes[1].innerText;
            id[1] = i.parentNode.childNodes[2].innerText;
            id[2] = i.parentNode.childNodes[3].innerText;
            id[3] = i.parentNode.childNodes[4].innerText;
            var xhtml = new XMLHttpRequest();
            xhtml.onreadystatechange = function () {
                if(this.readyState === 4 && this.status === 200) {
                    console.log(this.response);
                }
            };
            xhtml.open('POST','../controller/Update/updUser.php', true);
            var xtl='d0='+id[0]+'&d1='+id[1]+'&d2='+id[2]+'&d3='+id[3];
            xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhtml.send(xtl);
        }
        else{
            alert("Must be alphanumeric");
        }
    }
}
function cancel(rw2){
    var i = rw2.parentNode;
    i.innerHTML = "";
    i.innerText = valu;
    i.setAttribute("onclick","changecell(this)");
    saved=1;
    location.reload();
}
var saved = 0;
var valu;
function changecell(rw){
    if(saved != 1){
        var i = rw;
        valu = i.innerText;
        i.removeAttribute("onclick");
        i.innerHTML = "<input id='txt8' type='text'>" + "</br>" + "<a onclick='save(this)' href=\"#\" style='color: darkcyan'>Save</a>    "  + "<a style='color: darkcyan' onclick='cancel(this)' href=\"#\">Cancel</a>";
    }
    saved = 0;
}
function validate(inppp){
    console.log(inppp);
    var letters = /^[A-Za-z\s\.]+$/;
    var alfnum = /^[A-Za-z0-9\s\.]+$/;
    var date = /^\d{1,5}[/-][0-1][0-9][/-][0-3][0-9]$/;
    var nipt = /^([A-Za-z])\d{8}([A-Za-z])$/;
    var inp = inppp;
    if(!isNaN(inp)){
        return 1; //"Is number"
    } else if(inp.length==0){
        return 2 ; //Is empty
    }else if(inp.match(letters)){
        return 4 ; //Is alfanum
    } else if(inp.match(alfnum)){
        return 3 ; //Is alphabetic
    }

    else{return 5 ;} //dont know
}
function Search(){
    var e = document.getElementById("type");
    var type = e.options[e.selectedIndex].value;
    var val = document.getElementById("txt11").value;
    var xhtml = new XMLHttpRequest();
    xhtml.onreadystatechange = function () {
        if(this.readyState === 4 && this.status === 200) {
            var resp = this.response;
            var x = document.getElementById("table1").rows.length;
            for(var z = 1;z<x;z++){
                document.getElementById("table1").deleteRow(1);
            }
            document.getElementById("table1").innerHTML+=resp;
            document.getElementById('search').style.display='none';
        }
    };
    xhtml.open('POST','../controller/Search/usersearch.php', true);
    var xtl='d1='+type+'&d2='+val;
    xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhtml.send(xtl);
}
function addrow(){
    document.getElementById("p1").innerText="";
    document.getElementById("p2").innerText="";
    document.getElementById("p3").innerText="";
    document.getElementById("p4").innerText="";
    document.getElementById("p5").innerText="";
    document.getElementById("p6").innerText="";
    document.getElementById("p7").innerText="";
    document.getElementById("p8").innerText="";
    document.getElementById("p9").innerText="";
    document.getElementById("p10").innerText="";

    var username = document.getElementById("txt1").value;
    if(validate(username) == 1 || validate(username)==2 || validate(username)==5 ) {
        document.getElementById("p1").innerText="Username isn't valid";
        return 0;
    }
    var password = document.getElementById("txt2").value;
    var e = document.getElementById("txt4");
    var level = e.options[e.selectedIndex].value;

    var name = document.getElementById("txt5").value;
    if (validate(name) !=4 ){
        document.getElementById("p4").innerText = "Name should be only letters";
        return 0;
    }

    var surname = document.getElementById("txt6").value;
    if (validate(surname)!=4){
        document.getElementById("p5").innerText="Surname should be only letters";
        return 0;
    }
    var birthday = document.getElementById("txt7").value;
    var bonus = document.getElementById("txt8").value;
    if (bonus != 0  && bonus !=1){
        document.getElementById("p7").innerText="Bonus should be 0 or 1";
        return 0;
    }

    var typeofBonus = document.getElementById("txt9").value;

    var phone = document.getElementById("txt10").value;
    if (validate(phone)!=1){
        document.getElementById("p9").innerText = "Phone Number should be only numbers"
        return 0;
    }
    var iban = document.getElementById("txt17").value;

    var xhtml = new XMLHttpRequest();
    xhtml.onreadystatechange = function () {
        if(this.readyState === 4 && this.status === 200) {
            setTimeout(location.reload(),4000);
        }
    };
    xhtml.open('POST','../controller/Insert/insUser.php', true);
    var xtl='d1='+username+'&d2='+password+'&d3='+level+'&d4='+name+'&d5='+surname+'&d6='+birthday+'&d7='+bonus+'&d8='+typeofBonus+'&d9='
    +phone+'&d10='+iban;
    xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhtml.send(xtl);

}

function addrow2(){
    document.getElementById("p1").innerText="";
    document.getElementById("p2").innerText="";
    document.getElementById("p3").innerText="";
    document.getElementById("p11").innerText="";
    document.getElementById("p12").innerText="";
    document.getElementById("p13").innerText="";
    document.getElementById("p14").innerText="";
    document.getElementById("p15").innerText="";

    var username = document.getElementById("txt1").value;
    if(validate(username) != 3 ) {
        document.getElementById("p1").innerText="Username isn't valid";
        return 0;
    }
    var password = document.getElementById("txt2").value;
    var e = document.getElementById("txt4");
    var level = e.options[e.selectedIndex].value;


    var name = document.getElementById("txt12").value;
    if (validate(name) !=4 ){
        document.getElementById("p11").innerText = "Name should be only letters";
        return 0;
    }

    var surname = document.getElementById("txt13").value;
    if (validate(surname)!=4){
        document.getElementById("p12").innerText="Surname should be only letters";
        return 0;
    }
    var age = document.getElementById("txt14").value;

    var phone = document.getElementById("txt15").value;
    if (validate(phone)!=1){
        document.getElementById("p9").innerText = "Phone Number should be only numbers"
        return 0;
    }
    var address = document.getElementById("txt16").value;

    var xhtml = new XMLHttpRequest();
    xhtml.onreadystatechange = function () {
        if(this.readyState === 4 && this.status === 200) {
            setTimeout(location.reload(),4000);
        }
    };
    xhtml.open('POST','../controller/Insert/insUserMan.php', true);
    var xtl='d1='+username+'&d2='+password+'&d3='+level+'&d4='+name+'&d5='+surname+'&d6='+age+'&d7='+phone+'&d8='+address;
    xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhtml.send(xtl);

}









function detajeRW(rw3){
    var id = rw3.parentNode.parentNode.childNodes[1].innerText;
    document.cookie = 'pid='+id;
    location.href='../view/detpro.php';
}
function selsortdet(colm,ifn){
    var table = document.getElementById("table1");
    var rows = table.rows;
    var x,y;
    var coln = colm;
    var ifn = ifn;

    for(var i = 1;i < (rows.length - 1); i++){
        var min=i;
        if(butclconc[parseInt(coln)] == 0){
            for(var j = i+1; j < rows.length; j++){
                x = rows[i].getElementsByTagName("td")[parseInt(coln)];
                y = rows[j].getElementsByTagName("td")[parseInt(coln)];
                if(ifn == 1){
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        min = j;
                    }
                }
                else{
                    if (Number(x.innerHTML) < Number(y.innerHTML)) {
                        min = j;
                    }
                }
            }
        }
        else{
            for(var j = i+1; j < rows.length; j++){
                x = rows[i].getElementsByTagName("td")[parseInt(coln)];
                y = rows[j].getElementsByTagName("td")[parseInt(coln)];
                if(ifn == 1){
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        min = j;
                    }
                }
                else{
                    if (Number(x.innerHTML) > Number(y.innerHTML)) {
                        min = j;
                    }
                }
            }

        }
        rows[i].parentNode.insertBefore(rows[min], rows[i]);
    }
    if(butclconc[parseInt(coln)] == 0){
        butclconc[0] = 0;
        var th = document.getElementsByTagName("th")[0].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[1] = 0;
        var th = document.getElementsByTagName("th")[1].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[2] = 0;
        var th = document.getElementsByTagName("th")[2].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[3] = 0;
        var th = document.getElementsByTagName("th")[3].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[4] = 0;
        var th = document.getElementsByTagName("th")[4].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[5] = 0;
        var th = document.getElementsByTagName("th")[5].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[6] = 0;
        var th = document.getElementsByTagName("th")[6].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[7] = 0;
        var th = document.getElementsByTagName("th")[7].lastChild;
        th.attributes[1].value = "../resources/baseline_unfold_more_black_18dp2.png";
        butclconc[parseInt(coln)] = 1;
        console.log("des");
        var th = document.getElementsByTagName("th")[coln].lastChild;
        if(coln == 0 || coln == 5){
            th.attributes[1].value = "../resources/des.png";
        }
        else{
            th.attributes[1].value = "../resources/asc.png";
        }

    }
    else {
        butclconc[0] = 0;
        butclconc[1] = 0;
        butclconc[2] = 0;
        butclconc[3] = 0;
        butclconc[4] = 0;
        butclconc[5] = 0;
        butclconc[6] = 0;
        butclconc[7] = 0;
        console.log("asc");
        var th = document.getElementsByTagName("th")[coln].lastChild;
        if(coln == 0 || coln == 5){
            th.attributes[1].value = "../resources/asc.png";
        }
        else{
            th.attributes[1].value = "../resources/des.png";
        }

    }

}

/*
function generateReport(){
    var date = document.getElementById("data").value;
    console.log(date);
            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2",
                title:{
                    text: "Sales"
                },
                axisY: {
                    title: "Amount"
                },
                data: [{
                    type: "column",
                    yValueFormatString: "#,##0.## leke",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
             }]
            });
            chart.render();
}
*/


function showFunc() {
    var x = document.getElementById("txt4").value;
    console.log(x);
    if (x == 1) {
        document.getElementById("agent").style.display = 'block';
        document.getElementById("manager").style.display='none';
    }
    else if (x == 2){
        document.getElementById("manager").style.display = 'block';
        document.getElementById("agent").style.display = 'none';

    }
}

function edit(rw5) {
    var i = rw5.parentNode;
    var j = i.parentNode.rowIndex;
    tbl = document.getElementById("table1").getElementsByTagName('tbody')[0];
    var id = tbl.rows[j].cells[0].innerHTML;
    var level = tbl.rows[j].cells[3].innerHTML;


    if (level == 1) {
        var xhtml = new XMLHttpRequest();
        xhtml.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                console.log(this.response);
                var array = this.responseText.split(" ");
                console.log(array);

                console.log(array[0]);
                document.getElementById("frm0").value = array[0];
                document.getElementById("frm1").value = array[1];
                document.getElementById("frm2").value = array[2];
                document.getElementById("frm3").value = array[3];
                document.getElementById("frm4").value = array[4];
                document.getElementById("frm5").value = array[5];
                document.getElementById("frm6").value = array[6];
                document.getElementById("frm7").value = array[7];
                document.getElementById("forma").style.display = 'block';
            }
        };
        xhtml.open('POST', '../controller/getAgAdm.php', true);
        var xtl = 'd1=' + id;
        xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhtml.send(xtl);
    }
    else if (level == 2){
        var xhtml = new XMLHttpRequest();
        xhtml.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                console.log(this.response);
                var array = this.responseText.split(" ");
                console.log(array);
                var response = this.response;
                console.log(response[5]);
                console.log(array[0]);
                document.getElementById("frm13").value = array[0];
                document.getElementById("frm8").value = array[1];
                document.getElementById("frm9").value = array[2];
                document.getElementById("frm10").value = array[3];
                document.getElementById("frm11").value = array[4];
                document.getElementById("frm12").value = array[5]+" "+ array[6];
                document.getElementById("forma2").style.display = 'block';
            }
        };
        xhtml.open('POST', '../controller/getManAdm.php', true);
        var xtl = 'd1=' + id;
        xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhtml.send(xtl);
    }
}