<?php

session_start();
if(isset($_SESSION['level']) && $_SESSION['level'] == 1){
    header("Location:./view/pgagent.php");
}
else if(isset($_SESSION['level']) && $_SESSION['level'] == 2){
    header("Location:./view/pgmanager.php");
}
else if(isset($_SESSION['level']) && $_SESSION['level'] == 1){
    header("Location:./view/pgadmin.php");
}
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>E-Manage System</title>
    <link href="./view/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class = "container">

    <h1>WE WILL HELP MANAGE YOUR BUSINESS</h1>
    <br />
    <p class="center">E-Managing System <br>
    </p>
    <br />
    <?=(isset($_SESSION['message'])?$_SESSION['message']:"")?>
    <div>
    <a href="#" onclick="document.getElementById('login').style.display='block'"; class="button">Login</a>
    </div>


    <div class="contact modal" style="margin:10px;" id="login">
            <div class="contact-main" style="padding:1em">
                <div class="bs-example" data-example-id="simple-horizontal-form">

                    <form class="modal-content animate form-horizontal " method="post" action="./model/login.php" style="text-align:center;" >
                        <span onclick="document.getElementById('login').style.display='none'" class="close" >&times;</span>
                        <div class="contact-top">
                            <?=(isset($_SESSION['message'])?$_SESSION['message']:"")?>
                            <p style="font-size:200%;">Fill your information</p>
                        </div>
                        <!--Username-->
                        <div class="form-group">
                            <label>Username</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username" placeholder="Username" >
                            </div>
                        </div>


                        <div class="form-group">
                            <label>Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password" placeholder="Password">
                            </div>
                        </div>
                        <br><br>

                        <button type="submit" name="submit" style=" position:relative; background-color:black; color:white; width:140px;height:60px;">Login</button>

                    </form>
                </div>
        </div>
    </div>




</div>
</body>
</html>