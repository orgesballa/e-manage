<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="styleAgent.css">
    <script type="text/javascript" src="../model/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../model/contpayments.js"></script>
    <meta charset="UTF-8">
    <title>My Salaries</title>
</head>
<body>
<?php
session_start();
include("../model/htemplate.php");
include("../controller/db.php");
$agent = $_SESSION['id'];
try {
    if ($_SESSION["level"] == 2) {
        print("<div class='contact modal' style='margin:10px;height: 450px' id='pie'>
            <div class='contact - main' style='padding:1em'>
                <div class='bs - example' data-example-id='simple - horizontal - form'>
                <span onclick=\"document.getElementById('pie') . style . display = 'none'\" class='close' style='margin: auto'>×</span>
                <div id='chartContainer' style='height: 400px;margin-top:20px'></div>
                </div>
            </div>
            </div>
        </div>");
        print("
    <button id=\"but1\" onclick=\"document.getElementById('pay').style.display='block'\">Pay Agents</button>
<button id=\"but2\" onclick=\" showTotal() \">Total Salaries spread in agents</button>
<table id=\"table1\">
    <tr>
        <th onclick=\"selSort(0,0)\">ID <img id=\"im1\" src=\"../resources/baseline_unfold_more_black_18dp2.png\" ></th>
        <th onclick=\"selSort(1,1)\">Agent Name<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(2,1)\">Agent Surname<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclose=\"selSort(2,1)\">Date of payment<img src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(3,1)\">Amount<img id=\"im2\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
    </tr>");
        include("../controller/getsalary.php");
        print("
</table>
");
    } else {
        session_destroy();
        header("Location:../model/logout.php");
    }
} catch (Exception $e) {
    session_destroy();
    header("Location:../model/logout.php");
} ?>

<script type="text/javascript" src="../model/fullscreen.js"></script>
<?php include("../model/ftemplate.php"); ?>
<script>function showTotal() {
        var arry;
        document.getElementById('pie').style.display = 'block';
        var xhtml = new XMLHttpRequest();
        xhtml.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                arry = JSON.parse(this.response);
                console.log(arry);
                var chart = new CanvasJS.Chart("chartContainer", {
                    theme: "light2", // "light1", "light2", "dark1", "dark2"
                    exportEnabled: true,
                    animationEnabled: true,
                    title: {
                        text: "Salaries Paid To Agents "
                    },
                    data: [{
                        type: "pie",
                        startAngle: 25,
                        toolTipContent: "<b>{label}</b>: {y}",
                        showInLegend: "true",
                        legendText: "{label}",
                        indexLabelFontSize: 16,
                        indexLabel: "{label} - {y} Leke",
                        dataPoints: arry
                    }]
                });
                chart.render();

            }
        };
        xhtml.open('POST', '../controller/repPayments.php', true);
        xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhtml.send();
    }
</script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>
