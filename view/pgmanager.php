<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="styleAgent.css">
    <meta charset="UTF-8">
    <title>Index</title>
</head>
<body>

<?php
session_start();
$id = $_SESSION['id'];
include("../model/htemplate.php");
if($_SESSION['level']!=2 || !isset($_SESSION['level'])){
    header("Location: ../model/logout.php");
}
include "../controller/getmanwh.php"
?>

<h2 align="center">Welcome to your dashboard Mr/Mrs. <?php echo $surname ?></h2>
<div class="flexing">
    <div id="f1_container">
        <div id="f1_card" class="shadow">
            <div class="front face">
                <a  href = "mytotsales.php" ><img style="width: 300px;height: 300px;" src="../resources/totalsales.png"/></a>
            </div>
            <div class="back face center">
                <a  href = "mytotsales.php" ><img src="../resources/TotalSalesBack.jpg" style="box-shadow: -5px 5px 5px;"></a>
            </div>
        </div>
    </div>

    <div id="f1_container">
        <div id="f1_card" class="shadow">
            <div class="front face">
                <a  href = "myagent.php" ><img style="width: 300px;height: 300px;" src="../resources/agentsicon.png"/></a>
            </div>
            <div class="back face center">
                <a  href = "myagent.php" ><img src="../resources/agents.jpg" style="box-shadow: -5px 5px 5px;"></a>
            </div>
        </div>
    </div>

    <div id="f1_container">
        <div id="f1_card" class="shadow">
            <div class="front face">
                <a  href = "myproduct.php" ><img style="width: 300px;height: 300px;" src="../resources/productsicon.png"/></a>
            </div>
            <div class="back face center">
                <a  href = "myproduct.php" ><img src="../resources/products.jpg" style="box-shadow: -5px 5px 5px;"></a>
            </div>
        </div>
    </div>
    <div id="f1_container">
        <div id="f1_card" class="shadow">
            <div class="front face">
                <a  href = "mypayments.php" ><img style="width: 300px;height: 300px;"src="../resources/salaryicon.png"/></a>
            </div>
            <div class="back face center">
                <a  href = "mypayments.php" ><img src="../resources/salaries.jpg" style="box-shadow: -5px 5px 5px;"></a>
            </div>
        </div>
    </div>
</div>
<?php include("../model/ftemplate.php"); ?>
<script src = "../model/jquery-3.3.1.min.js"></script>
<script src="../model/fullscreen.js"></script>
</body>
</html>