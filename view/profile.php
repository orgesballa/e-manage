<?php
session_start();
$level = $_SESSION['level'];
$id = $_SESSION['id'];
if ($level == 1) {
    include "../controller/getagjwh.php";
} else if ($level == 2) {
    header("Location: ../view/profile2.php");
}
else if ($level==3){
    header("Location: ../view/pgadmin.php");
}
else{
    header("Location: ../model/logout.php");
}
$imgname = $name . ".jpg";
?>
<!DOCTYPE html>
<html>
<head>
    <title>Agent</title>
    <script src="../model/jquery-3.3.1.min.js"></script>
    <script src="../model/fullscreen.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="profileStyle.css">
    <script src="../model/profileJS.js" type="text/javascript"></script>
    <title>Profile Page</title>
</head>
<body>
<?php include("../model/htemplate.php"); ?>
<div id="ProfilePage">
    <div id="LeftCol">
        <div id="Photo"><img src="../resources/<?php echo "default.jpg" ?>"></div>
        <div id="PhotoOptions">
            <a download="default.jpg" href="../resources/default.jpg" title="Logo title">Download Photo</a>
        </div>
    </div>
    <div id="Info">
        <p>
            <strong>Name:</strong>
            <span><?php echo "$name" ?></span>
        </p>
        <p>
            <strong>Surname:</strong>
            <span><?php echo "$surname" ?></span>
        </p>
        <p>
            <strong>Birthday:</strong>
            <span><?php echo "$age" ?></span>
        </p>
        <p>
            <strong>Bonus:</strong>
            <span><?php echo "$bonus" ?></span>
        </p>
        <p>
            <strong>Lloji i bonusit:</strong>
            <span><?php echo "$ctgbon" ?></span>
        </p>
        <p>
            <strong>Phone #:</strong>
            <span><?php echo $phone ?></span>
        </p>
        <p>
            <strong>IBAN</strong>
            <span><?php echo "$iban" ?></span>
        </p>
    </div>
<table id="table1">
    <tr>
        <th onclick="selSort(0,0)">Date of Payment<img id="im1" src="../resources/baseline_unfold_more_black_18dp.png">
        </th>
        <th onclick="selSort(1,1)">Agent<img id="im6" src="../resources/baseline_unfold_more_black_18dp.png"></th>
        <th onclick="selSort(2,1)">Value<img id="im6" src="../resources/baseline_unfold_more_black_18dp.png"></th>
    </tr>
    <?php include "../controller/getpade.php" ?>
</table>

<button id="btnTotal" onclick="calcTotal()">Calculate Total Payment</button>
<label id="total" style="margin-top: 100px;margin-left: 90px; font-size: 14pt;color:black;"></label>
</div>
<?php include("../model/ftemplate.php"); ?>
</body>
</html>

