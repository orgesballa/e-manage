<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="styleAgent.css">
    <script type='text/javascript' src='../model/jquery-3.3.1.min.js'></script>
    <meta charset="UTF-8">
    <title>Total Sales</title>
</head>
<body>
<?php
session_start();
include("../model/htemplate.php");
include("../controller/db.php");
$agent=$_SESSION['id'];
try{
    if($_SESSION["level"]==2){
        print("
<button id=\"but1\" onclick=\"document.getElementById('login').style.display='block'\">Add a record</button>
<button id=\"but2\" onclick=\"document.getElementById('search').style.display='block'\">Search</button>
<button id='pdfBut'>Print PDF</button>
<table id=\"table1\">
    <tr>
        <th onclick=\"selsortdet(0,0)\">ID<img id=\"im1\" src=\"../resources/baseline_unfold_more_black_18dp2.png\" ></th>
        <th onclick=\"selsortdet(1,1)\">Date of Sale<img id=\"im1\" src=\"../resources/baseline_unfold_more_black_18dp2.png\" ></th>
        <th onclick=\"selsortdet(2,1)\">Company<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(3,1)\">NIPT<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(4,1)\">Code<img id=\"im2\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(5,1)\">Brand<img id=\"im3\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(6,0)\">Quantity in Stock<img id=\"im4\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(7,0)\">Quantity Sold<img id=\"im5\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(8,0)\">Price<img id=\"im5\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(9,0)\">Sold Price<img id=\"im5\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>       
        <th onclick=\"selsortdet(10,1)\">Agent Name<img id=\"im5\" src=\"../resources/baseline_unfold_more_black_18dp.png\"></th>       
        <th onclick=\"selsortdet(11,1)\">Agent Surname<img id=\"im5\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>       
    </tr>");
        include("../controller/getshde.php");
        print("
</table>
");
    }
    else{
        session_destroy();
        header("Location:../model/logout.php");
    }
}
catch(Exception $e){
    session_destroy();
    header("Location:../model/logout.php");
}?>

<?php include "../model/ftemplate.php"; ?>
<script type="text/javascript" src="../model/fullscreen.js"></script>
<script src="../model/conttsale.js"></script>
</body>
</html>