<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="styleAgent.css">
    <script  src='../model/jquery-3.3.1.min.js'></script>
    <script src="../model/contkli.js"></script>
    <meta charset="UTF-8">
    <title>My Clients</title>
</head>
<body>

<?php
session_start();
include("../model/htemplate.php");
?>
<h1> My Clients Dashboard</h1>
<?php
include("../controller/db.php");
$agent=$_SESSION['id'];
try{
if($_SESSION["level"]==1){
print("

<button id=\"but1\" onclick=\"document.getElementById('login').style.display='block'\">Add a client</button>
<button id=\"but2\" onclick=\"document.getElementById('search').style.display='block'\">Search</button>
<table id=\"table1\">
    <tr>
        <th onclick=\"selSort(0,0)\">ID <img id=\"im1\" src=\"../resources/baseline_unfold_more_black_18dp2.png\" ></th>
        <th onclick=\"selSort(1,1)\">Company<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(2,1)\">NIPT<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(3,0)\">Mobile #<img id=\"im2\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(4,1)\">Address<img id=\"im3\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(5,1)\">Agent Name<img id=\"im4\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(6,1)\">Agent Surname<img id=\"im5\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>        
    </tr>");
    include("../controller/getkliwh.php");
    print("
</table>
");
}
else{
session_destroy();
header("Location:../model/logout.php");
}
}
catch(Exception $e){
session_destroy();
header("Location:../model/logout.php");
}?>
<?php include "../model/ftemplate.php";?>
<script src="../model/fullscreen.js"></script>
</body>
</html>