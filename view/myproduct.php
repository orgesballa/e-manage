<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="styleAgent.css">
    <script type="text/javascript" src="../model/jquery-3.3.1.min.js"></script>
    <meta charset="UTF-8">
    <title>My Products</title>
</head>
<body>
<?php
session_start();
include("../model/htemplate.php");
include("../controller/db.php");
$agent=$_SESSION['id'];
try{
    if($_SESSION["level"]==2){
        print("
  <h1>Products Dashboard</h1>
<button id=\"but1\" onclick=\"document.getElementById('login').style.display='block'\">Add a product</button>
<button id=\"but2\" onclick=\"document.getElementById('search').style.display='block'\">Search</button>
<button id='pdfBut'>Print PDF</button>
<table id=\"table1\">
    <tr>
        <th onclick=\"selSort(0,0)\">ID <img id=\"im1\" src=\"../resources/baseline_unfold_more_black_18dp2.png\" ></th>
        <th onclick=\"selSort(1,1)\">Code<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(2,1)\">Brand<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(3,1)\">Detail<img id=\"im2\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(4,1)\">Unit<img id=\"im3\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(5,0)\">Bought Price<img id=\"im4\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(6,0)\">Sold Price<img id=\"im5\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>        
        <th onclick=\"selSort(7,0)\">Quantity in Stock<img id=\"im5\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>        
        <th onclick=\"selSort(8,0)\">Min. Quantity<img id=\"im5\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>        
    
    </tr>");
        include("../controller/getprodukt.php");
        print("
</table>
");
    }
    else{
        session_destroy();
        header("Location:../model/logout.php");
    }
}
catch(Exception $e){
    session_destroy();
    header("Location:../model/logout.php");
}?>
<script type="text/javascript" src="../model/fullscreen.js"></script>
<script src="../model/contprod.js"></script>
<?php include("../model/ftemplate.php"); ?>
</body>

</html>