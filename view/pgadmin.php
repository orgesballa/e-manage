<?php
session_start();
if($_SESSION['level']!=3 || !isset($_SESSION['level'])){
header("Location: ../model/logout.php");
}?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="styleAgent.css">
    <meta charset="UTF-8">
    <title>Admin Page</title>
</head>
<body>
<?php include("../model/htemplate.php"); ?>

<h2 align="center">Admin Dashboard</h2>
    <div id="f1_container" style="position: center;margin:auto">
        <div id="f1_card" class="shadow">
            <div class="front face">
                <a  href = "myusers.php" ><img style="width: 300px;height: 300px;" src="../resources/adminPanel.png"/></a>
            </div>
            <div class="back face center">
                <a  href = "myusers.php" ><img src="../resources/crud.png" style="box-shadow: -5px 5px 5px;"></a>
            </div>
        </div>
    </div>

<?php include "../model/ftemplate.php"; ?>
<script src="../model/jquery-3.3.1.min.js"></script>
<script src="../model/fullscreen.js"></script>
</body>
</html>