<!DOCTYPE html>
<html lang="en">
<head>
    <script src="../model/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="styleAgent.css">
    <script src="../model/fullscreen.js" type="text/javascript"></script>
    <title>Agent</title>
</head>
<body>
<?php
session_start();
include("../model/htemplate.php");
if($_SESSION['level']!=1 || !isset($_SESSION['level'])){
    header("Location: ../model/logout.php");
}
$id = $_SESSION['id'];
include "../controller/getagjwh.php" ?>


<h2 align="center">Welcome to your dashboard Mr/Mrs. <?php echo $surname ?></h2>
<div class="flexing">
<div id="f1_container">
    <div id="f1_card" class="shadow">
        <div class="front face">
            <a  href = "mysales.php" ><img style="width: 300px;height: 300px;" src="../resources/salesicon.png"/></a>
        </div>
        <div class="back face center">
            <a  href = "mysales.php" ><img src="../resources/mysales.jpg" style="box-shadow: -5px 5px 5px;"></a>
        </div>
    </div>
</div>

<div id="f1_container">
    <div id="f1_card" class="shadow">
        <div class="front face">
            <a  href = "myclients.php" ><img style="width: 300px;height: 300px;" src="../resources/clientsicon.png"/></a>
        </div>
        <div class="back face center">
            <a  href = "myclients.php" ><img src="../resources/myclients.jpg" style="box-shadow: -5px 5px 5px;"></a>
        </div>
    </div>
</div>

<div id="f1_container">
    <div id="f1_card" class="shadow">
        <div class="front face">
            <a  href = "reports.php" ><img style="width: 300px;height: 300px;"src="../resources/reporticon.png"/></a>
        </div>
        <div class="back face center">
            <a  href = "reports.php" ><img src="../resources/myreport.jpg" style="box-shadow: -5px 5px 5px;"></a>
        </div>
    </div>
</div>
</div>
</body>

<?php include "../model/ftemplate.php"; ?>

</html>