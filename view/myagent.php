<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="styleAgent.css">
    <script type="text/javascript" src="../model/jquery-3.3.1.min.js"></script>
    <script src="../model/contagje.js"></script>
    <meta charset="UTF-8">
    <title>My Agents</title>
</head>
<body>
<?php
session_start();
include("../model/htemplate.php");
include("../controller/db.php");
$agent=$_SESSION['id'];
try{
    if($_SESSION["level"]==2){
        print("<div class='contact modal' style='margin:10px;height: 450px' id='rep'>
            <div class='contact - main' style='padding:1em'>
                <div class='bs - example' data-example-id='simple - horizontal - form'>
                <span onclick=\"document.getElementById('rep') . style . display = 'none'\" class='close' style='margin: auto'>×</span>
                <div id='chartContainer' style='height: 400px;margin-top:20px'></div>
                </div>
            </div>
            </div>
        </div>");
        print("
<button id=\"but1\" onclick=\"document.getElementById('search').style.display='block'\">Search</button>
<button id=\"but2\" onclick=\" generateReport() \">Reports</button>
<label>  Date for Report:</label>
<input type='date' id='data'>
<table id=\"table1\">
    <tr>
        <th onclick=\"selSort(0,0)\">ID <img id=\"im1\" src=\"../resources/baseline_unfold_more_black_18dp2.png\" ></th>
        <th onclick=\"selSort(1,1)\">Name<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(2,1)\">Surname<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(3,1)\">Birthday<img id=\"im2\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(4,0)\">Bonus<img id=\"im2\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(5,1)\">Type of Payment<img id=\"im3\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(6,1)\">Mobile #<img id=\"im3\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(7,1)\">IBAN<img id=\"im3\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th> 
    </tr>");
        include("../controller/getagjent.php");
        print("
</table>
");
    }
    else{
        session_destroy();
        header("Location:../model/logout.php");
    }
}
catch(Exception $e){
    session_destroy();
    header("Location:../model/logout.php");
}?>
<script>function generateReport(){
        var arry;
        document . getElementById('rep') . style . display = 'block';
        var date = document.getElementById("data").value;
        var xhtml = new XMLHttpRequest();
        xhtml.onreadystatechange = function () {
            if(this.readyState === 4 && this.status === 200) {
                console.log(this.response);
                arry = JSON.parse(this.response);
                var chart = new CanvasJS.Chart("chartContainer", {
                    animationEnabled: true,
                    exportEnabled: true,
                    theme: "light2",
                    title:{
                        text: "Sales for this period of time"
                    },
                    axisY: {
                        title: "Amount"
                    },
                    data: [{
                        type: "column",
                        yValueFormatString: "#,##0.## leke",
                        dataPoints: arry
                    }]
                });
                chart.render();
            }
        };
        xhtml.open('POST','../controller/getrepwh2.php', true);
        var xtl='data='+date;
        xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhtml.send(xtl);
    }
</script>
<script src="../model/canvasjs.min.js"></script>
<script type="text/javascript" src="../model/fullscreen.js"></script>
<?php include("../model/ftemplate.php"); ?>
</body>
</html>