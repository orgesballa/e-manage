<?php
session_start();
include("../model/htemplate.php");
include "../controller/db.php";
$agent=$_COOKIE['pid'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="styleAgent.css">
    <meta charset="UTF-8">
    <title>Products Details</title>

</head>
<body>
<h1>Sales made of this product</h1>
<?php

try{
    if($_SESSION["level"]==2){

        print("<div class='contact modal' style='margin:10px;height: 450px' id=\"search\">
            <div class=\"contact - main\" style=\"padding:1em\">
                <div class=\"bs - example\" data-example-id=\"simple - horizontal - form\">
                <span onclick=\"document.getElementById('search') . style . display = 'none'\" class=\"close\">×</span>
                <div id='chartContainer' style='height: 400px;'></div>
                </div>
            </div>
            </div>
        </div>");
        print("
<button id=\"but1\" onclick=\"location.href='../view/myproduct.php'\">Back</button>
<button id=\"but2\" onclick=\"generateReport()\">Reports</button>
<table id=\"table1\">
    <tr>
        <th onclick=\"selsortdet(0,1)\">Date<img id=\"im1\" src=\"../resources/baseline_unfold_more_black_18dp2.png\" ></th>
        <th onclick=\"selsortdet(1,0)\">Quantity<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(2,0)\">Price<img id=\"im2\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(3,0)\">Total Price<img id=\"im3\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(4,1)\">Agent Name<img id=\"im4\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(5,1)\">Agent Surname<img id=\"im5\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(6,1)\">Client<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(7,1)\">Nipt<img id=\"im7\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
    </tr>");
        $sql = "SELECT shitje.datash,shitje.sasi,shitje.cmimi,shitje.vlere,agjent.emri,agjent.mbiemri,klient.Kompania,klient.Nipt FROM `shitje` INNER JOIN agjent ON shitje.agjenti=agjent.id2 INNER JOIN klient ON shitje.klienti=klient.id1 INNER JOIN produkte ON shitje.produkti=produkte.id WHERE produkte.id =" .$agent;
        $result = $conn->query($sql);
        while($arres = mysqli_fetch_assoc($result)) {
            print("<tr>
        <td>".$arres['datash']."</td><td>".$arres['sasi']."</td><td>".$arres['cmimi']."</td><td>".$arres['vlere']."</td><td>".$arres['emri']."</td><td>".$arres['mbiemri']."</td><td>".$arres['Kompania']."</td><td>".$arres['Nipt']."</td></tr>");
        }
        $conn->close();
        print("
</table>
<label>Date for Report:</label>
<input type='date' id='data'>
<script src=\"../model/jquery-3.3.1.min.js\"></script>
<script src=\"../model/contprod.js\"></script>
  <script type=\"text/javascript\" src=\"../model/fullscreen.js\"></script>
<script src=\"../model/canvasjs.min.js\"></script>
");
    }
    else{
        session_destroy();
        header("Location:../model/logout.php");
    }
}
catch(Exception $e){
    session_destroy();
    header("Location:../model/logout.php");
}?>
<?php include("../model/ftemplate.php"); ?>
</body>
<script>function generateReport(){
        var arry;
        document . getElementById('search') . style . display = 'block';
        var date = document.getElementById("data").value;
        var xhtml = new XMLHttpRequest();
        xhtml.onreadystatechange = function () {
            if(this.readyState === 4 && this.status === 200) {
                arry = JSON.parse(this.response);
                var chart = new CanvasJS.Chart("chartContainer", {
                    animationEnabled: true,
                    theme: "light2",
                    title:{
                        text: "Sales for this period of time"
                    },
                    axisY: {
                        title: "Amount"
                    },
                    data: [{
                        type: "column",
                        yValueFormatString: "#,##0.## leke",
                        dataPoints: arry
                    }]
                });
                chart.render();
            }
        };
        xhtml.open('POST','../controller/getrepwh.php', true);
        var xtl='data='+date+'&pid=<?php echo $agent ?>' ;
        xhtml.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhtml.send(xtl);
    }
</script>
</html>
