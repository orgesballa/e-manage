<?php
session_start();
$level = $_SESSION['level'];
$id = $_SESSION['id'];
if ($level == 2) {
    include "../controller/getmanwh.php";
}
else {
    header("../controller/logout.php");
}

$imgname = $name . ".jpg";
?>
<!DOCTYPE html>
<html>
<head>
    <title>Agent</title>
    <script src="../model/jquery-3.3.1.min.js"></script>
    <script src="../model/fullscreen.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="profileStyle.css">
    <script src="../model/profileJS.js" type="text/javascript"></script>
    <title>Profile Page</title>
</head>
<body>
<?php include("../model/htemplate.php"); ?>
<div id="ProfilePage">
    <div id="LeftCol">
        <div id="Photo"><img src="../resources/<?php echo "default.jpg" ?>"></div>
        <div id="PhotoOptions">
            <a download="default.jpg" href="../resources/default.jpg" title="Logo title">Download Photo</a>
        </div>
    </div>
    <div id="Info">
        <p>
            <strong>Name:</strong>
            <span><?php echo "$name" ?></span>
        </p>
        <p>
            <strong>Surname:</strong>
            <span><?php echo "$surname" ?></span>
        </p>
        <p>
            <strong>Age:</strong>
            <span><?php echo "$age" ?></span>
        </p>
        <p>
            <strong>Phone #:</strong>
            <span><?php echo $phone ?></span>
        </p>
        <p>
            <strong>Address</strong>
            <span><?php echo "$address" ?></span>
        </p>
    </div>
</div>
<div style="margin-top: 500px;">
    <?php include("../model/ftemplate.php"); ?>
</div>
</body>
</html>

