<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="styleAgent.css">
    <script type="text/javascript" src="../model/fullscreen.js"></script>
    <meta charset="UTF-8">
    <title>Index</title>
</head>
<body>
<?php
session_start();
include("../model/htemplate.php");
include "../controller/db.php";
$agent=$_COOKIE['id'];
try{
if($_SESSION["level"]==1){
    print("
<button id=\"but1\" onclick=\"location.href='../view/myclients.php'\">Back</button>
<table id=\"table1\">
    <tr>
        <th onclick=\"selsortdet(0,0)\">Date<img id=\"im1\" src=\"../resources/baseline_unfold_more_black_18dp2.png\" ></th>
        <th onclick=\"selsortdet(1,0)\">Quantity<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(2,0)\">Quantity in Stock<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(3,0)\">Price<img id=\"im2\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(4,0)\">Total Price<img id=\"im3\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(5,1)\">Agent Name<img id=\"im4\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(6,1)\">Agent Surname<img id=\"im5\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(7,1)\">Kode<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selsortdet(8,1)\">Brand<img id=\"im7\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
    </tr>");
$sql = "SELECT produkte.Sasigj,shitje.datash,shitje.sasi,shitje.cmimi,shitje.vlere,agjent.emri,agjent.mbiemri,produkte.Kodi,produkte.Brandi FROM `shitje` INNER JOIN agjent ON shitje.agjenti=agjent.id2 INNER JOIN klient ON shitje.klienti=klient.id1 INNER JOIN produkte ON shitje.produkti=produkte.id WHERE klient.id1 =" . $agent;
$result = $conn->query($sql);
while($arres = mysqli_fetch_assoc($result)) {
    print("<tr>
        <td>".$arres['datash']."</td><td>".$arres['sasi']."</td><td>".$arres['Sasigj']."</td><td>".$arres['cmimi']."</td><td>".$arres['vlere']."</td><td>".$arres['emri']."</td><td>".$arres['mbiemri']."</td><td>".$arres['Kodi']."</td><td>".$arres['Brandi']."</td></tr>");
}
$conn->close();
    print("
</table>
<script src=\"../model/contkli.js\"></script>
");
}
else{
    session_destroy();
    header("Location:../model/logout.php");
}
}
catch(Exception $e){
    session_destroy();
    header("Location:../model/logout.php");
}?>
<?php include("../model/ftemplate.php"); ?>
</body>
</html>



