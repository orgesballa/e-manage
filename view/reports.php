<?php
session_start();
if ($_SESSION['level']!=1){
    header("Location: ../model/logout.php");
}
include("../controller/db.php");
$id = $_SESSION['id'];

$result = mysqli_query($conn,"select * from shitje where agjenti = '$id'");

$dataPoints = array();
while ($row = mysqli_fetch_assoc($result)) {
    $arr = array("y" => $row['vlere'],"label"=>$row['id3']);
    array_push($dataPoints,$arr);
}
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="profileStyle.css">
    <script src="../model/fullscreen.js"></script>
    <meta charset="UTF-8">
    <title>Index</title>
    <script>
        window.onload = function() {

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2",
                title:{
                    text: "Sales"
                },
                axisY: {
                    title: "Amount"
                },
                data: [{
                    type: "column",
                    yValueFormatString: "#,##0.## leke",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();

        }
    </script>
</head>
<body>
<?php
include("../model/htemplate.php");

$sql1 = "SELECT SUM(vlere) as total FROM `shitje` WHERE agjenti = '$id'";
$result = mysqli_query($conn,$sql1);
$row=mysqli_fetch_assoc($result);
$totalInc = $row['total'];

$sql2 = "SELECT SUM(sasi) as prodSold FROM `shitje` WHERE agjenti = '$id'";
$result2 = mysqli_query($conn,$sql2);
$row2=mysqli_fetch_assoc($result2);
$totalPro = $row2['prodSold'];

$sql3 = "SELECT COUNT(id1) as noCli FROM `klient` where agentid = '$id'";
$result3 = mysqli_query($conn,$sql3);
$row3 = mysqli_fetch_assoc($result3);
$noCli = $row3['noCli'];


$sql4= "select Sum(vlere) as total,klienti from shitje where klienti IN (select id1 from klient where agentid = '$id') GROUP BY klienti DESC order by total DESC";
$result4 = mysqli_query($conn,$sql4);
$row4 = mysqli_fetch_assoc($result4);
$total = $row4['total'];
$klientID = $row4['klienti'];

$sql5 = "select klient.Kompania from klient where id1 = '$klientID'";
$result5 = mysqli_query($conn,$sql5);
$row5 = mysqli_fetch_assoc($result5);
$kompania = $row5['Kompania'];



?>
<h1>PERFORMANCE DASHBOARD</h1>

<div class = "allboxes">
    <div class = "box1">
            <p style="color: white">TOTAL <strong style="font-size: 18pt;font-family: 'Arial','sans-serif'">INCOME</strong></p>
            <p style="font-size: 24pt;text-align: center;color: white"><?php if (isset($totalInc)) echo $totalInc." Leke";
                else echo "0 Lekë"?></p>
            <!--<div style="width: inherit;padding-left:20px;background-color: #888888;color: white;text-align: center;">Yearly Stats for Agent</div>-->
    </div>
    <div class = "box2">
        <p style="color: white">TOTAL <strong style="font-size: 18pt;font-family: 'Arial','sans-serif'">PRODUCTS SOLD</strong></p>
        <p style="font-size: 24pt;text-align: center;color: white"><?php if (isset($totalPro)) echo $totalPro;
                            else echo "0"?></p>
    </div>
    <div class = "box3">
        <p style="color: white">NUMBER OF <strong style="font-size: 18pt;font-family: 'Arial','sans-serif'">CLIENTS</strong></p>
        <p style="font-size: 24pt;text-align: center;color: white"><?php if (isset($noCli)) echo $noCli;
                        else echo "0"?></p>
    </div>
    <div class = "box4">
        <p style="color: white">TOP <strong style="font-size: 18pt;font-family: 'Arial','sans-serif'">BUYING CLIENT</strong></p>
        <p style="font-size: 24pt;text-align: center;color: white"><?php if (isset($kompania))
                        echo $kompania." ".$total." Lekë";
                        else echo "None"?></p>
    </div>
</div>








<div id="chart">
    <div id="chartContainer" style="height: 370px; width: 60%;left:25%;position: absolute"></div>



</div>

<div id="stats">


</div>

<script src="../model/canvasjs.min.js"></script>
</body>
</html>