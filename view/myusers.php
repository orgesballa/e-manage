<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="styleAgent.css">
    <script src='../model/jquery-3.3.1.min.js'></script>
    <script src="../model/contuser.js"></script>
    <meta charset="UTF-8">
    <title>My Users</title>
</head>
<body>
<?php
session_start();
include("../model/htemplate.php");
include("../controller/db.php");
$id = $_SESSION['id'];
try {
    if ($_SESSION["level"] == 3) {
        print("

<button id=\"but1\" onclick=\"document.getElementById('login').style.display='block'\">Add an user</button>
<button id=\"but2\" onclick=\"document.getElementById('search').style.display='block'\">Search</button>
<div class='flexing'>
<div>
<table id=\"table1\">
    <tr>
        <th onclick=\"selSort(0,0)\">ID <img id=\"im1\" src=\"../resources/baseline_unfold_more_black_18dp2.png\" ></th>
        <th onclick=\"selSort(1,1)\">Username<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(2,1)\">Password<img id=\"im6\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
        <th onclick=\"selSort(3,0)\">Level<img id=\"im2\" src=\"../resources/baseline_unfold_more_black_18dp2.png\"></th>
    </tr>");
        include("../controller/getuserwh.php");
        print("
</table>
</div>
<div>
<form style='display:none;' id='forma' action='../controller/Update/updProf.php' method='post'>
                
                
                <input type='number' value='' style='display: none;' name = 'id' id ='frm0'>
                <label>Name:</label>
                <input id=\"frm1\" type=\"text\" value='' name = 'name'>
                <p id='err1' style='color: red'></p>
                
                <label>Surname:</label>
                <input id=\"frm2\" type=\"text\" value='' name = 'surname'>
                <p id='err2' style='color: red'></p>
                
                <label>Birthday:</label>
                <input id=\"frm3\" type=\"date\" name = 'birthday'>
                <p id='err3' style='color: red'></p>
                
                <label>Bonus:</label>
                <input id=\"frm4\" type=\"number\" name = 'bonus'>
                <p id='err4' style='color: red'></p>
                
                <label>Type of Bonus:</label>
                <input id=\"frm5\" type=\"text\" name = 'ctgbon'>
                <p id='err5' style='color: red'></p>
                
                <label>Phone Number #:</label>
                <input id =\"frm6\" type='text' name = 'phone'>
                <p id='err6' style='color: red'></p>
                
                <label>IBAN</label>
                <input id=\"frm7\" type=\"text\" name = 'iban'>
                <p id='err7' style='color: red'></p>
                 <button id=\"but4\" type='submit'>Edit</button>
                 <button id=\"but5\" onclick=\"document.getElementById('forma').style.display='none'\">Cancel</button>
</form>
</div>
<div>
                <form id=\"forma2\" style='display:none;' method='post' action='../controller/Update/updManager.php'>       
                 <input type='number' value='' style='display: none;' name = 'id2' id ='frm13'>
                <label>Name:</label>
                <input id=\"frm8\" type=\"text\" name ='name2'>
                <p id='p11' style='color: red'></p>
                
                <label>Surname:</label>
                <input id=\"frm9\" type=\"text\" name ='surname2'> 
                <p id='p12' style='color: red'></p>
                
                <label>Age:</label>
                <input id=\"frm10\" type=\"number\" name = 'age2'>
                <p id='p13' style='color: red'></p>
                
                <label>Phone:</label>
                <input id=\"frm11\" type=\"text\" name ='phone2'>
                <p id='p14' style='color: red'></p>
                
                <label>Address:</label>
                <input id=\"frm12\" type=\"text\" name = 'address'>
                <p id='p15' style='color: red'></p>
                
               
                <button id=\"but6\" type='submit'>Edit</button>
                <button id=\"but7\" onclick=\"document.getElementById('forma2').style.display='none'\">Cancel</button>
                </form>


</div>






</div>
");
    } else {
        session_destroy();
        header("Location:../model/logout.php");
    }
} catch (Exception $e) {
    session_destroy();
    header("Location:../model/logout.php");
} ?>
<?php include "../model/ftemplate.php"; ?>
<script src="../model/fullscreen.js"></script>
</body>
</html>